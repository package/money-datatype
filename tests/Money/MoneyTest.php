<?php
/**
 * User: thorsten
 * Date: 07.07.16
 * Time: 20:30
 */

namespace Tests\Bnet\Money;


use Bnet\Money\Currency;
use Bnet\Money\Money;
use Bnet\Money\MoneyException;
use Bnet\Money\Repositories\ArrayRepository;
use Bnet\Money\TaxedMoney;

class MoneyTest extends \PHPUnit_Framework_TestCase {


	/**
	 * @param $amount
	 * @param null $currency
	 * @return Money
	 */
	public function money($amount, $currency = null) {
		return new Money($amount, $currency);
	}

	/**
	 * Sets up the fixture, for example, open a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		parent::setUp();
		$r = new ArrayRepository([
			'EUR' => [
				'code' => 'EUR',
				'iso' => 978,
				'name' => 'Euro',
				'symbol_left' => '',
				'symbol_right' => '€',
				'decimal_place' => 2,
				'decimal_mark' => ',',
				'thousands_separator' => '.',
				'unit_factor' => 100
			],
			'USD' => [
				'code' => 'USD',
				'decimal_place' => 2,
				'decimal_mark' => '.',
				'thousands_separator' => ',',
			]
		]);
		Currency::registerCurrencyRepository($r);
	}

    public function testBasicFunctions() {
        $amount = 123456;
        $m = $this->money($amount, $this->currency());
        $this->assertEquals($amount, $m->amount(), 'Amount');
        $this->assertEquals($amount, $m->value(), 'Amount');
        $this->assertEquals("1234.56", $m->normalize(), 'Normalize');
        $this->assertEquals('1234,56€', $m->format(), 'default Format');

        $m2 = Money::fromFloat(1234.56);
        $this->assertEquals($m2->amount(), $m->amount());

        try {
            Money::fromFloat(1234);
        } catch (\Exception $e) {
            print_r($e->getMessage());
        }
    }

    public function testConstructorCurrencyAssert() {
        $amount = 123456;
        $m = $this->money($amount, $this->currency());

        // asserts are OK
        $this->money($m, null);
        $this->money($m, 'EUR');
        $this->money($m, $this->currency());

        try {
            $this->money($m, 'USD');
            $this->fail('No Exception of asserting USD is not EUR');
        } catch (\InvalidArgumentException $e) {
            $x = 'Different currencies';
            $this->assertTrue(substr($e->getMessage(), 0, strlen($x)) === $x, 'correct ExceptionMessage');
        }
        try {
            $this->money($m, new Currency('USD'));
            $this->fail('No Exception of asserting USD is not EUR 2');
        } catch (\InvalidArgumentException $e) {
            $x = 'Different currencies';
            $this->assertTrue(substr($e->getMessage(), 0, strlen($x)) === $x, 'correct ExceptionMessage');
        }
    }

    public function testConstructorWithMoneyObj() {
		$amount = 123456;
		$m = $this->money($amount, $this->currency());

		$newMoney = new Money($m);
		$this->assertEquals($newMoney->amount(), $m->amount(), 'Amount');
		$this->assertEquals($newMoney->currency(), $m->currency(), 'Currency');

        try {
            $wrongCurrency = new Currency('USD');
            new Money($m, $wrongCurrency);
            $this->fail('No Exception on float-string');
        } catch (\InvalidArgumentException $e) {
            $x = 'Different currencies';
            $this->assertTrue(substr($e->getMessage(), 0, strlen($x)) === $x, 'correct ExceptionMessage');
        }

        $this->assertTrue(true);
	}

	protected function currency() {
		$c = new Currency('EUR', [
			'code' => 'EUR',
			'iso' => 978,
			'name' => 'Euro',
			'symbol_left' => '',
			'symbol_right' => '€',
			'decimal_place' => 2,
			'decimal_mark' => ',',
			'thousands_separator' => '.',
			'unit_factor' => 100
		]);
		return $c;
	}

	public function testFormat() {
		$amount = 123456;
		$m = $this->money($amount, $this->currency());

		$this->assertEquals('1234,56€', $m->format(), 'default Format');
		$this->assertEquals('1.234,56€', $m->format(true), 'Format +thPt');

		$this->assertEquals('€1234,56', $m->format(false, false, true), 'Format Swap');
		$this->assertEquals('€1.234,56', $m->format(true, false, true), 'Format +thPt Swap');

		$this->assertEquals('1234,56 EUR', $m->format(false, true), 'Format code');
		$this->assertEquals('1.234,56 EUR', $m->format(true, true), 'Format code +thPt');

		$this->assertEquals('EUR 1234,56', $m->format(false, true, true), 'Format code swap');
		$this->assertEquals('EUR 1.234,56', $m->format(true, true, true), 'Format code swap +thPt');

	}

	public function testHtml() {
		$amount = 123456;
		$m = $this->money($amount, $this->currency());

		$this->assertEquals('<span class="money currency_eur"><span class="amount">1234,56</span><span class="symbol">€</span></span>', $m->html(), 'default Html');
		$this->assertEquals('<span class="money currency_eur"><span class="amount">1.234,56</span><span class="symbol">€</span></span>', $m->html(true), 'Html +thPt');

		$this->assertEquals('<span class="money currency_eur"><span class="symbol">€</span><span class="amount">1234,56</span></span>', $m->html(false, false, true), 'Html Swap');
		$this->assertEquals('<span class="money currency_eur"><span class="symbol">€</span><span class="amount">1.234,56</span></span>', $m->html(true, false, true), 'Html +thPt Swap');

		$this->assertEquals('<span class="money currency_eur"><span class="amount">1234,56</span> <span class="code">EUR</span></span>', $m->html(false, true), 'Html code');
		$this->assertEquals('<span class="money currency_eur"><span class="amount">1.234,56</span> <span class="code">EUR</span></span>', $m->html(true, true), 'Html code +thPt');

		$this->assertEquals('<span class="money currency_eur"><span class="code">EUR</span> <span class="amount">1234,56</span></span>', $m->html(false, true, true), 'Html code swap');
		$this->assertEquals('<span class="money currency_eur"><span class="code">EUR</span> <span class="amount">1.234,56</span></span>', $m->html(true, true, true), 'Html code swap +thPt');

	}

	public function testNoNumber() {
		try {
			$this->money(11.1, $this->currency());
			$this->fail('No Exception on float');
		} catch (MoneyException $e) { }

		try {
			$this->money('11.1', $this->currency());
			$this->fail('No Exception on float-string');
		} catch (MoneyException $e) { }

		$this->assertTrue(true);
	}


	public static function provideStringsMoneyParsing() {
		$usd = Currency::USD();
		$eur = Currency::EUR();
		return [
			[$usd, "1000", 100000],
			[$usd, "1000.0", 100000],
			[$usd, "1000.00", 100000],
			[$usd, "1000.1", 100010],
			[$usd, "1000.11", 100011],
			[$eur, "1000,0", 100000],
			[$eur, "1000,00", 100000],
			[$eur, "1000,1", 100010],
			[$eur, "1000,11", 100011],
			[$eur, "1.000,11", 100011],
			[$usd, "1,000.11", 100011],
			[$usd, "0.01", 1],
			[$eur, "0,01", 1],
			[$eur, "1", 100],
			[$usd, "1", 100],
			[$eur, "-1000", -100000],
			[$usd, "-1000", -100000],
			[$usd, "-1000.0", -100000],
			[$usd, "-1000.00", -100000],
			[$usd, "-0.01", -1],
			[$eur, "-1000,0", -100000],
			[$eur, "-1000,00", -100000],
			[$eur, "-0,01", -1],
			[$eur, "-1", -100],
			[$usd, "-1", -100],
			[$eur, "+1000", 100000],
			[$usd, "+1000", 100000],
			[$usd, "+1000.0", 100000],
			[$usd, "+1000.00", 100000],
			[$usd, "+0.01", 1],
			[$eur, "+1000,0", 100000],
			[$eur, "+1000,00", 100000],
			[$eur, "+0,01", 1],
			[$eur, "+1", 100],
			[$usd, "+1", 100],
			[$eur, "4.321.000,11", 432100011],
			[$usd, "4,321,000.11", 432100011],
			[$eur, "7.654.321.000,11", 765432100011],
			[$usd, "7,654,321,000.11", 765432100011],
			[$eur, "7654.321.000,11", 765432100011],
			[$usd, "7654,321,000.11", 765432100011],

			// parsing without currency
			[null, "1000", 100000],
			[null, "1000.0", 100000],
			[null, "1000.00", 100000],
			[null, "1000.1", 100010],
			[null, "1000.11", 100011],
			[null, "1000,0", 100000],
			[null, "1000,00", 100000],
			[null, "1000,1", 100010],
			[null, "1000,11", 100011],
			[null, "1.000,11", 100011],
			[null, "1.000.11", 100011],
			[null, "1,000,11", 100011],
			[null, "1,000.11", 100011],
			[null, "0.01", 1],
			[null, "0,01", 1],
			[null, "1", 100],
			[null, "1", 100],
			[null, "-1000", -100000],
			[null, "-1000", -100000],
			[null, "-1000.0", -100000],
			[null, "-1000.00", -100000],
			[null, "-0.01", -1],
			[null, "-1000,0", -100000],
			[null, "-1000,00", -100000],
			[null, "-0,01", -1],
			[null, "-1", -100],
			[null, "-1", -100],
			[null, "+1000", 100000],
			[null, "+1000", 100000],
			[null, "+1000.0", 100000],
			[null, "+1000.00", 100000],
			[null, "+0.01", 1],
			[null, "+1000,0", 100000],
			[null, "+1000,00", 100000],
			[null, "+0,01", 1],
			[null, "+1", 100],
			[null, "+1", 100],
		];
	}

	/**
	 * test parsing of money strings
	 * @param Currency|null $currency
	 * @param $string
	 * @param $units
	 * @dataProvider provideStringsMoneyParsing
	 */
	public function testMoneyParsing($currency, $string, $units) {
		$m = $this->money($units);
		try {
		    // test parsing after normal or the currency format
            $parsed = $currency ? Money::parseAfterCurrencyFormat($string, $currency) : Money::parse($string);
            $this->assertEquals($m->value(), $parsed->value(), 'Value: ' . $string);
		} catch (\Exception $e) {
			$this->fail('Exception on Value: ' . $string . ' -> ' . $e->getMessage());
		}
	}

	/**
	 * test parsing of money int/float
	 * @param Currency|null $currency
	 * @param string $string unused
     * @param  int|float  $amount
     * @dataProvider provideStringsMoneyParsing
	 */
	public function testMoneyFromFloat($currency, $string, $amount) {
	    $currency = $currency ?? new Currency();
        // calculate to float
        $float = (float)($amount / $currency->unit_factor);
        // generate reference MOneyObj
        $m = $this->money($amount);
        try {
		    // test parsing after normal or the currency format
            $parsed = Money::fromFloat($float, $currency);
            $this->assertEquals($m->value(), $parsed->value(), 'Value: ' . $string);
		} catch (\Exception $e) {
			$this->fail('Exception on Value: ' . $float . ' -> ' . $e->getMessage());
		}
	}

	public function testFactoryMethods() {
		$this->assertEquals(Money::EUR(25), Money::EUR(10)->add(Money::EUR(15)));
		$this->assertEquals(Money::USD(25), Money::USD(10)->add(Money::USD(15)));
	}

	/**
	 * @expectedException \Bnet\Money\MoneyException
	 */
	public function testStringThrowsException() {
		$this->money('foo', new Currency('EUR'));
	}

	public function testGetters() {
		$m = $this->money(100, new Currency('EUR'));
		$this->assertEquals(100, $m->amount());
		$this->assertEquals(1, $m->normalize());
		$this->assertEquals(new Currency('EUR'), $m->currency());
	}

	public function testSameCurrency() {
		$m = $this->money(100, new Currency('EUR'));
		$this->assertTrue($m->isSameCurrency($this->money(100, new Currency('EUR'))));
		$this->assertFalse($m->isSameCurrency($this->money(100, new Currency('USD'))));
	}

	public function testComparison() {
		$m1 = $this->money(50, new Currency('EUR'));
		$m2 = $this->money(100, new Currency('EUR'));
		$m3 = $this->money(200, new Currency('EUR'));
		$this->assertEquals(-1, $m2->compare($m3));
		$this->assertEquals(1, $m2->compare($m1));
		$this->assertEquals(0, $m2->compare($m2));
		$this->assertTrue($m2->equals($m2));
		$this->assertFalse($m3->equals($m2));
		$this->assertTrue($m3->greaterThan($m2));
		$this->assertFalse($m2->greaterThan($m3));
		$this->assertTrue($m2->greaterThanOrEqual($m2));
		$this->assertFalse($m2->greaterThanOrEqual($m3));
		$this->assertTrue($m2->lessThan($m3));
		$this->assertFalse($m3->lessThan($m2));
		$this->assertTrue($m2->lessThanOrEqual($m2));
		$this->assertFalse($m3->lessThanOrEqual($m2));
	}

	/**
	 * @expectedException \InvalidArgumentException
	 */
	public function testDifferentCurrenciesCannotBeCompared() {
		$m1 = $this->money(100, new Currency('EUR'));
		$m2 = $this->money(100, new Currency('USD'));
		$m1->compare($m2);
	}

	public function testAddition() {
		$m1 = $this->money(1100101, new Currency('EUR'));
		$m2 = $this->money(1100021, new Currency('EUR'));
		$sum = $m1->add($m2);
		$this->assertEquals($this->money(2200122, new Currency('EUR')), $sum);
		$this->assertNotEquals($sum, $m1);
		$this->assertNotEquals($sum, $m2);
	}

	/**
	 * @expectedException \InvalidArgumentException
	 */
	public function testDifferentCurrenciesCannotBeAdded() {
		$m1 = $this->money(100, new Currency('EUR'));
		$m2 = $this->money(100, new Currency('USD'));
		$m1->add($m2);
	}

	public function testAdditionForTaxedAndNonTaxedObjects() {
        $money = new Money(1100101, new Currency('EUR'));
        $withTax = TaxedMoney::fromNet(1000020, 10, new Currency('EUR'));

		$sum1 = $money->add($withTax);
		$this->assertEquals(new Money(2200123, new Currency('EUR')), $sum1);
        $this->assertEquals(2200123, $sum1->amount());
        $this->assertNotEquals($sum1, $money);
		$this->assertNotEquals($sum1, $withTax);

		$sum2 = $withTax->add($money);
		$this->assertEquals(2200123, $sum2->amount());
        $this->assertNotEquals($sum2, $money);
        $this->assertNotEquals($sum2, $withTax);
        $this->assertEquals($sum1->amount(), $sum2->amount());
    }

	public function testSubtraction() {
		$m1 = $this->money(10010, new Currency('EUR'));
		$m2 = $this->money(10002, new Currency('EUR'));
		$diff = $m1->subtract($m2);
		$this->assertEquals($this->money(8, new Currency('EUR')), $diff);
		$this->assertNotSame($diff, $m1);
		$this->assertNotSame($diff, $m2);
	}

	/**
	 * @expectedException \InvalidArgumentException
	 */
	public function testDifferentCurrenciesCannotBeSubtracted() {
		$m1 = $this->money(100, new Currency('EUR'));
		$m2 = $this->money(100, new Currency('USD'));
		$m1->subtract($m2);
	}

	public function testMultiplication() {
		$m1 = $this->money(15, new Currency('EUR'));
		$m2 = $this->money(1, new Currency('EUR'));
		$this->assertEquals($m1, $m2->multiply(15));
		$this->assertNotEquals($m1, $m2->multiply(10));
	}

	public function testDivision() {
		$m1 = $this->money(3, new Currency('EUR'));
		$m2 = $this->money(10, new Currency('EUR'));
		$this->assertEquals($m1, $m2->divide(3));
		$this->assertNotEquals($m1, $m2->divide(2));
	}

	public function testAllocation() {
		$m1 = $this->money(100, new Currency('EUR'));
		list($part1, $part2, $part3) = $m1->allocate([1, 1, 1]);
		$this->assertEquals($this->money(34, new Currency('EUR')), $part1);
		$this->assertEquals($this->money(33, new Currency('EUR')), $part2);
		$this->assertEquals($this->money(33, new Currency('EUR')), $part3);
		$m2 = $this->money(101, new Currency('EUR'));
		list($part1, $part2, $part3) = $m2->allocate([1, 1, 1]);
		$this->assertEquals($this->money(34, new Currency('EUR')), $part1);
		$this->assertEquals($this->money(34, new Currency('EUR')), $part2);
		$this->assertEquals($this->money(33, new Currency('EUR')), $part3);
	}

	public function testAllocationOrderIsImportant() {
		$m = $this->money(5, new Currency('EUR'));
		list($part1, $part2) = $m->allocate([3, 7]);
		$this->assertEquals($this->money(2, new Currency('EUR')), $part1);
		$this->assertEquals($this->money(3, new Currency('EUR')), $part2);
		list($part1, $part2) = $m->allocate([7, 3]);
		$this->assertEquals($this->money(4, new Currency('EUR')), $part1);
		$this->assertEquals($this->money(1, new Currency('EUR')), $part2);
	}

	public function testComparators() {
		$m1 = $this->money(0, new Currency('EUR'));
		$m2 = $this->money(-1, new Currency('EUR'));
		$m3 = $this->money(1, new Currency('EUR'));
		$m4 = $this->money(1, new Currency('EUR'));
		$m5 = $this->money(1, new Currency('EUR'));
		$m6 = $this->money(-1, new Currency('EUR'));
		$this->assertTrue($m1->isZero());
		$this->assertTrue($m2->isNegative());
		$this->assertTrue($m3->isPositive());
		$this->assertFalse($m4->isZero());
		$this->assertFalse($m5->isNegative());
		$this->assertFalse($m6->isPositive());
	}

	public function testEmptyAmounts() {
		$this->money(0);
		$this->money('');
		$this->money(null);
		$this->money(0.00);
		$this->assertTrue(true);
	}

    public function testAbs() {
	    $this->assertEquals($this->money(12300)->abs(), $this->money(12300));
	    $this->assertEquals($this->money(-12300)->abs(), $this->money(12300));
	    $this->assertEquals($this->money(12312)->abs(), $this->money(12312));
	    $this->assertEquals($this->money(-12312)->abs(), $this->money(12312));
    }

}
